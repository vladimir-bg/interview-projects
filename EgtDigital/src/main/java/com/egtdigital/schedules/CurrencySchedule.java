package com.egtdigital.schedules;

import com.egtdigital.models.Currency;
import com.egtdigital.models.Dates;
import com.egtdigital.models.Rates;
import com.egtdigital.service.contracts.CurrencyService;
import com.egtdigital.service.contracts.DatesService;
import com.egtdigital.service.contracts.NameService;
import com.egtdigital.service.contracts.RatesService;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

@Component
public class CurrencySchedule {
    private final CurrencyService currencyService;
    private final NameService nameService;
    private final RestTemplate restTemplate;
    private final DatesService datesService;
    private final RatesService ratesService;

    private final String kay = "5d70ebf0f06d6b66bab53e5baa3743f1";
    private final String url = "http://data.fixer.io/api/latest?access_key=" + kay;

    public CurrencySchedule(CurrencyService currencyService, NameService nameService, RestTemplateBuilder restTemplateBuilder, DatesService datesService, RatesService ratesService) {
        this.currencyService = currencyService;
        this.nameService = nameService;
        this.restTemplate = restTemplateBuilder.build();
        this.datesService = datesService;
        this.ratesService = ratesService;
    }

    //@Scheduled(cron = "0 0 */3 * * *", zone = "Europe/Sofia")
    @Scheduled(fixedRate = 10000, initialDelay = 1000)
    public void getLatestCurrenciesJob() throws JSONException, ParseException {
        JSONObject jsonObject = new JSONObject(restTemplate.getForObject(url, String.class));
        JSONObject rates = jsonObject.optJSONObject("rates");
        JSONArray key = rates.names();

        Currency currency = new Currency();
        currency.setDates(datesService.create(getDateFromJson(jsonObject.optString("date"))));
        currencyService.create(currency);
        currency.setRates(rateSet(key, rates, currency));
        currencyService.update(currency);
    }

    private Dates getDateFromJson(String dateString) throws ParseException {
        Dates date = new Dates();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        date.setDate(formatter.parse(dateString));
        return date;
    }

    private Set<Rates> rateSet(JSONArray key, JSONObject rates, Currency currency) throws JSONException {
        Set<Rates> ratesSet = new HashSet<>();
        for (int i = 0; i < key.length (); ++i) {
            String keys = key.getString(i);
            Rates rate = new Rates();
            rate.setCurrency(currency);
            rate.setNames(nameService.getName(key.getString(i)));
            rate.setValue((float) rates.getDouble(keys));
            ratesSet.add(ratesService.create(rate));
        }
        return ratesSet;
    }
}