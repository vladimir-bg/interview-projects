package com.egtdigital.repository;

import com.egtdigital.exceptions.EntityNotFoundException;
import com.egtdigital.models.Names;
import com.egtdigital.repository.contracts.NameRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NameRepositoryImpl implements NameRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public NameRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Names getName(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Names> query = session.createQuery("from Names as n " +
                    "where n.name = :name", Names.class);
            query.setParameter("name", name);
            List<Names> result = query.list();
            if(result == null) throw new EntityNotFoundException("name not found");
            return result.get(0);
        }
    }
}
