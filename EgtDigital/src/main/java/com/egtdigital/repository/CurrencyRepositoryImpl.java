package com.egtdigital.repository;

import com.egtdigital.models.Currency;
import com.egtdigital.models.Rates;
import com.egtdigital.repository.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyRepositoryImpl implements CurrencyRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Rates getLast(String name) {
        try (Session session = sessionFactory.openSession()){
            Query<Rates> query = session.createQuery(
                    "select r " +
                    "from Rates as r " +
                    "join r.names as n " +
                    "join r.currency as c " +
                    "join c.dates as d " +
                    "where n.name = :name " +
                    "order by d.date DESC ", Rates.class);
            query.setParameter("name", name);
            query.setMaxResults(1);
            return query.uniqueResult();
        }
    }

    @Override
    public List<Rates> getLatestInPeriod(String name, int period) {
        try (Session session = sessionFactory.openSession()){
            Query<Rates> query = session.createQuery(
                    "select r " +
                            "from Rates as r " +
                            "join r.names as n " +
                            "join r.currency as c " +
                            "join c.dates as d " +
                            "where n.name = :name " +
                            "order by d.date DESC ", Rates.class);
            query.setParameter("name", name);
            query.setMaxResults(period / 3);
            return query.getResultList();
        }
    }

    @Override
    public void update(Currency currency) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(currency);
            session.getTransaction().commit();
        }

    }

    @Override
    public void create(Currency currency) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(currency);
            session.getTransaction().commit();
        }
    }
}
