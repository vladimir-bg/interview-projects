package com.egtdigital.repository;

import com.egtdigital.models.Rates;
import com.egtdigital.repository.contracts.RatesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RatesRepositoryImpl implements RatesRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RatesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Rates create(Rates rates) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(rates);
            session.getTransaction().commit();
        }
        return rates;
    }
}
