package com.egtdigital.repository;

import com.egtdigital.models.Dates;
import com.egtdigital.repository.contracts.DatesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DatesRepositoryImpl implements DatesRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public DatesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Dates create(Dates dates) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(dates);
            session.getTransaction().commit();
        }
        return dates;
    }
}
