package com.egtdigital.repository.contracts;

import com.egtdigital.models.Dates;

public interface DatesRepository {
    Dates create(Dates dates);
}
