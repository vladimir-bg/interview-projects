package com.egtdigital.repository.contracts;

import com.egtdigital.models.Rates;

public interface RatesRepository {
    Rates create(Rates rates);
}
