package com.egtdigital.repository.contracts;

import com.egtdigital.models.History;

public interface HistoryRepository {
    boolean checkRequestId(String id);
    void create(History history);
}
