package com.egtdigital.repository.contracts;

import com.egtdigital.models.Currency;
import com.egtdigital.models.Rates;

import java.util.List;

public interface CurrencyRepository {
    Rates getLast(String name);
    List<Rates> getLatestInPeriod(String name, int period);
    void update(Currency currency);
    void create(Currency currency);
}
