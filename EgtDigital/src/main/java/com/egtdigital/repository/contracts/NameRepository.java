package com.egtdigital.repository.contracts;

import com.egtdigital.models.Names;

public interface NameRepository {
    Names getName(String name);
}
