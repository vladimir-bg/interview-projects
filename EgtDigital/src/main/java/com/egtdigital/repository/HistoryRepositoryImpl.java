package com.egtdigital.repository;

import com.egtdigital.models.History;
import com.egtdigital.repository.contracts.HistoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class HistoryRepositoryImpl implements HistoryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public HistoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean checkRequestId(String id) {
        try (Session session = sessionFactory.openSession()){
            Query<History> query = session.createQuery("from History as h " +
                    "where h.requestId = :id", History.class);
            query.setParameter("id", id);
            return query.list() != null;
        }
    }

    @Override
    public void create(History history) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(history);
            session.getTransaction().commit();
        }
    }
}
