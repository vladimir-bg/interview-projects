package com.egtdigital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EgtDigitalApplication {
    public static void main(String[] args) {
        SpringApplication.run(EgtDigitalApplication.class, args);
    }
}
