package com.egtdigital.models.response;

public class ResponseCurrency {
    String currency;
    float value;

    public ResponseCurrency() {
    }

    public ResponseCurrency(String currency, float value) {
        setCurrency(currency);
        setValue(value);
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
