package com.egtdigital.web.dtos;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="command")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(CurrencyXmlDto.class)
public class CommandXmlDto {
    @XmlAttribute(name="id")
    private String id;

    @XmlElement(name = "get")
    private GetXmlDto getXmlDto;

    @XmlElement(name = "history")
    private HistoryXmlDto historyXmlDto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GetXmlDto getGetXmlDto() {
        return getXmlDto;
    }

    public void setGetXmlDto(GetXmlDto getXmlDto) {
        this.getXmlDto = getXmlDto;
    }

    public HistoryXmlDto getHistoryXmlDto() {
        return historyXmlDto;
    }

    public void setHistoryXmlDto(HistoryXmlDto historyXmlDto) {
        this.historyXmlDto = historyXmlDto;
    }
}
