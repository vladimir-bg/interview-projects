package com.egtdigital.web.dtos;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="history")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(HistoryXmlDto.class)
public class HistoryXmlDto {
    @XmlAttribute(name = "consumer")
    private int consumer;

    @XmlAttribute(name = "period")
    private int period;

    @XmlAttribute(name = "currency")
    private String currency;

    public int getConsumer() {
        return consumer;
    }

    public void setConsumer(int consumer) {
        this.consumer = consumer;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
