package com.egtdigital.web.dtos;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="get")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(GetXmlDto.class)
public class GetXmlDto {
    @XmlAttribute(name = "consumer")
    private int consumer;

    @XmlElement(name = "currency")
    private CurrencyXmlDto currencyXmlDto;

    public int getConsumer() {
        return consumer;
    }

    public void setConsumer(int consumer) {
        this.consumer = consumer;
    }

    public CurrencyXmlDto getCurrencyXmlDto() {
        return currencyXmlDto;
    }

    public void setCurrencyXmlDto(CurrencyXmlDto currencyXmlDto) {
        this.currencyXmlDto = currencyXmlDto;
    }
}
