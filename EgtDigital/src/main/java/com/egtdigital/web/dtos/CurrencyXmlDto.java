package com.egtdigital.web.dtos;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="currency")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(CurrencyXmlDto.class)
public class CurrencyXmlDto {
    @XmlValue
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
