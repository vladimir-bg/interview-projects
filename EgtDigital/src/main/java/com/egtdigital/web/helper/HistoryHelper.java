package com.egtdigital.web.helper;

import com.egtdigital.models.History;
import com.egtdigital.service.contracts.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class HistoryHelper {
    private final HistoryService historyService;

    @Autowired
    public HistoryHelper(HistoryService historyService) {
        this.historyService = historyService;
    }

    public void createHistory(String service, String requestId, int clientId) {
        History history = new History();
        history.setClientId(clientId);
        history.setRequestId(requestId);
        history.setTime(new Date());
        history.setServiceName(service);
        historyService.create(history);
    }
}
