package com.egtdigital.web.controllers;

import com.egtdigital.service.contracts.CurrencyService;
import com.egtdigital.service.contracts.HistoryService;
import com.egtdigital.web.dtos.CommandXmlDto;
import com.egtdigital.web.helper.HistoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/xml_api")
public class XmlController {
    private final CurrencyService currencyService;
    private final HistoryService historyService;
    private final HistoryHelper historyHelper;

    @Autowired
    public XmlController(CurrencyService currencyService, HistoryService historyService, HistoryHelper historyHelper) {
        this.currencyService = currencyService;
        this.historyService = historyService;
        this.historyHelper = historyHelper;
    }

    @PostMapping("/command")
    public ResponseEntity<?> test(@Valid @RequestBody CommandXmlDto commandXmlDto) {
        if(historyService.checkRequestId(commandXmlDto.getId()))
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Duplicated request id");
        if(commandXmlDto.getHistoryXmlDto() != null) {
            historyHelper.createHistory("xml_hist",
                    commandXmlDto.getId(),
                    commandXmlDto.getHistoryXmlDto().getConsumer());
            return new ResponseEntity<>(currencyService.getByPeriod(commandXmlDto.getHistoryXmlDto().getCurrency(),
                    commandXmlDto.getHistoryXmlDto().getPeriod()),
                    HttpStatus.OK);
        } else if (commandXmlDto.getGetXmlDto() != null) {
            historyHelper.createHistory("xml_curr",
                    commandXmlDto.getId(),
                    commandXmlDto.getGetXmlDto().getConsumer());
            return new ResponseEntity<>(currencyService.getMostRecent(commandXmlDto.getHistoryXmlDto().getCurrency()),
                    HttpStatus.OK);
        }
        return null;
    }
}
