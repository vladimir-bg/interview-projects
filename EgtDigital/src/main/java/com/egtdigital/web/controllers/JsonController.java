package com.egtdigital.web.controllers;

import com.egtdigital.models.response.ResponseCurrency;
import com.egtdigital.service.contracts.CurrencyService;
import com.egtdigital.service.contracts.HistoryService;
import com.egtdigital.web.dtos.CurrencyDto;
import com.egtdigital.web.dtos.CurrencyHistoryDto;
import com.egtdigital.web.helper.HistoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/json_api")
public class JsonController {
    private final CurrencyService currencyService;
    private final HistoryService historyService;
    private final HistoryHelper historyHelper;

    @Autowired
    public JsonController(CurrencyService currencyService, HistoryService historyService, HistoryHelper historyHelper) {
        this.currencyService = currencyService;
        this.historyService = historyService;
        this.historyHelper = historyHelper;
    }

    @PostMapping("/current")
    public ResponseCurrency current(@Valid @RequestBody CurrencyDto currencyDto) {
        if(historyService.checkRequestId(currencyDto.getRequestId()))
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Duplicated request id");

        historyHelper.createHistory("json_curr", currencyDto.getRequestId(), currencyDto.getClient());
        return currencyService.getMostRecent(currencyDto.getCurrency());
    }

    @PostMapping("/history")
    public List<ResponseCurrency> history(@Valid @RequestBody CurrencyHistoryDto currencyHistoryDto) {
        if(historyService.checkRequestId(currencyHistoryDto.getRequestId()))
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Duplicated request id");

        historyHelper.createHistory("json_hist", currencyHistoryDto.getRequestId(), currencyHistoryDto.getClient());
        return currencyService.getByPeriod(currencyHistoryDto.getCurrency(), currencyHistoryDto.getPeriod());
    }
}
