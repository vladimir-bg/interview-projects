package com.egtdigital.service;

import com.egtdigital.models.Names;
import com.egtdigital.repository.contracts.NameRepository;
import com.egtdigital.service.contracts.NameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NameServiceImpl implements NameService {
    private final NameRepository nameRepository;

    @Autowired
    public NameServiceImpl(NameRepository nameRepository) {
        this.nameRepository = nameRepository;
    }

    @Override
    public Names getName(String name) {
        return nameRepository.getName(name);
    }
}
