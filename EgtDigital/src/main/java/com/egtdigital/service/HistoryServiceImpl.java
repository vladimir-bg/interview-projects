package com.egtdigital.service;

import com.egtdigital.models.History;
import com.egtdigital.repository.contracts.HistoryRepository;
import com.egtdigital.service.contracts.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService {
    private final HistoryRepository historyRepository;

    @Autowired
    public HistoryServiceImpl(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    @Override
    public boolean checkRequestId(String id) {
        return historyRepository.checkRequestId(id);
    }

    @Override
    public void create(History history) {
        historyRepository.create(history);
    }
}
