package com.egtdigital.service;

import com.egtdigital.models.Currency;
import com.egtdigital.models.Rates;
import com.egtdigital.models.response.ResponseCurrency;
import com.egtdigital.repository.contracts.CurrencyRepository;
import com.egtdigital.service.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public ResponseCurrency getMostRecent(String currencyType) {
        Rates rate = currencyRepository.getLast(currencyType);
        return new ResponseCurrency(rate.getNames().getName(), rate.getValue());
    }

    @Override
    public List<ResponseCurrency> getByPeriod(String currencyType, int period) {
        List<Rates> rates = currencyRepository.getLatestInPeriod(currencyType, period);
        List<ResponseCurrency> response = new ArrayList<>();
        for (Rates rateResponse : rates)
            response.add(new ResponseCurrency(rateResponse.getNames().getName(), rateResponse.getValue()));
        return response;
    }

    @Override
    public void update(Currency currency) {
        currencyRepository.update(currency);
    }

    @Override
    public void create(Currency currency) {
        currencyRepository.create(currency);
    }
}
