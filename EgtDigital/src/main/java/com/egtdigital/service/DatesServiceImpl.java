package com.egtdigital.service;

import com.egtdigital.models.Dates;
import com.egtdigital.repository.contracts.DatesRepository;
import com.egtdigital.service.contracts.DatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DatesServiceImpl implements DatesService {
    private final DatesRepository datesRepository;

    @Autowired
    public DatesServiceImpl(DatesRepository datesRepository) {
        this.datesRepository = datesRepository;
    }

    @Override
    public Dates create(Dates dates) {
        return datesRepository.create(dates);
    }
}
