package com.egtdigital.service;

import com.egtdigital.models.Rates;
import com.egtdigital.repository.contracts.RatesRepository;
import com.egtdigital.service.contracts.RatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatesServiceImpl implements RatesService {
    private final RatesRepository ratesRepository;

    @Autowired
    public RatesServiceImpl(RatesRepository ratesRepository) {
        this.ratesRepository = ratesRepository;
    }

    @Override
    public Rates create(Rates rates) {
        return ratesRepository.create(rates);
    }
}
