package com.egtdigital.service.contracts;

import com.egtdigital.models.Rates;

public interface RatesService {
    Rates create(Rates rates);
}
