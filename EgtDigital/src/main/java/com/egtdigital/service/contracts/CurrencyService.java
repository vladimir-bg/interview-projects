package com.egtdigital.service.contracts;

import com.egtdigital.models.Currency;
import com.egtdigital.models.response.ResponseCurrency;

import java.util.List;

public interface CurrencyService {
    ResponseCurrency getMostRecent(String currencyType);
    List<ResponseCurrency> getByPeriod(String currencyType, int period);
    void update(Currency currency);
    void create(Currency currency);
}
