package com.egtdigital.service.contracts;

import com.egtdigital.models.Dates;

public interface DatesService {
    Dates create(Dates dates);
}
