package com.egtdigital.service.contracts;

import com.egtdigital.models.History;

public interface HistoryService {
    boolean checkRequestId(String id);
    void create(History history);
}
