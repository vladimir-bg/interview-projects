package com.egtdigital.service.contracts;

import com.egtdigital.models.Names;

public interface NameService {
    Names getName(String name);
}
