CREATE DATABASE IF NOT EXISTS `gateway`;
USE 'gateway';

create or replace table dates
(
    id   int auto_increment
        primary key,
    date datetime null
);

create or replace table currency
(
    id      int auto_increment
        primary key,
    date_id int not null,
    constraint currency_date_id_fk
        foreign key (date_id) references dates (id)
);

create or replace table names
(
    id   int auto_increment
        primary key,
    name varchar(10) not null
);

create or replace table rates
(
    id          int auto_increment
        primary key,
    value       float not null,
    name_id     int   not null,
    currency_id int   not null,
    constraint rates_currency_fk
        foreign key (currency_id) references currency (id),
    constraint rates_names_fk
        foreign key (name_id) references names (id)
);

create or replace table history
(
    id           int auto_increment
        primary key,
    service_name varchar(10) not null,
    request_id   varchar(32) not null,
    time         date        not null,
    client_id    int         not null
);