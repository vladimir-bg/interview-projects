package parsers;

import contracts.FunctionTypeJson;
import exceptions.DuplicateFileException;
import models.Employee;
import models.Report;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import parsers.utils.GenerateFileLocation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class JsonFileParser {
    public static void readEmployeeFile(String fileName, List<Employee> employees) throws IOException, ParseException {
        FunctionTypeJson functionType = (file) -> {
            JSONArray jsonEmployeeArray = (JSONArray) file;
            for (Object emp : jsonEmployeeArray) {
                employees.add(parseEmployeeObject((JSONObject) emp));
            }
        };
        readFile(fileName, functionType);
    }

    public static void readReportFile(String fileName, Report report) throws IOException, ParseException {
        FunctionTypeJson functionType = (file) -> parseReportObject(report, (JSONObject) file);
        readFile(fileName, functionType);
    }

    private static Employee parseEmployeeObject(JSONObject employeeJsonObject) {
        Employee employee = new Employee();
        employee.setName((String) employeeJsonObject.get("name"));
        employee.setTotalSales((long) employeeJsonObject.get("totalSales"));
        employee.setSalesPeriod((long) employeeJsonObject.get("salesPeriod"));
        employee.setExperienceMultiplier((double) employeeJsonObject.get("experienceMultiplier"));
        return employee;
    }

    private static void parseReportObject(Report report, JSONObject reportJsonObject) {
        report.setTopPerformersThreshold((long) reportJsonObject.get("topPerformersThreshold"));
        report.setUseExperienceMultiplier((boolean) reportJsonObject.get("useExperienceMultiplier"));
        report.setPeriodLimit((long) reportJsonObject.get("periodLimit"));
    }

    private static void readFile(String fileName, FunctionTypeJson functionType) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        try(FileReader file = new FileReader(GenerateFileLocation.generate(fileName, "json"))) {
            Object parsedFile = jsonParser.parse(file);
            functionType.run(parsedFile);
        } catch (FileNotFoundException | DuplicateFileException e) {
            throw new FileNotFoundException("File with " + fileName + " not found");
        } catch (ParseException e) {
            throw new ParseException(0);
        } catch (IOException  e) {
            throw new IOException(e.getMessage());
        }
    }
}
