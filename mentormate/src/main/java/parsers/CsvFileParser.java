package parsers;

import exceptions.DuplicateFileException;
import models.Employee;
import models.Report;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import parsers.utils.GenerateFileLocation;

import java.io.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

public class CsvFileParser {
    public static void create(Report report, List<Employee> employeeList, String fileName) throws DuplicateFileException, IOException {
        if(duplicateFileName(fileName))
            throw new DuplicateFileException("File with " + fileName + " already exists");

        File file = new File(GenerateFileLocation.generate(fileName, "csv"));
        try(
                FileWriter fileWriter = new FileWriter(file);
                CSVPrinter csvPrinter = new CSVPrinter(fileWriter,
                        CSVFormat.DEFAULT.withHeader("Name", "Score"))
        ) {
            List<Employee> employees = employeeResult(employeeList, report);
            for (Employee employee : employees)
                csvPrinter.printRecord(employee.getName(), calculateScore(employee, report));
            csvPrinter.flush();
        }
    }

    private static boolean duplicateFileName(String fileName) throws IOException {
        try {
            new FileReader(GenerateFileLocation.generate(fileName, "csv"));
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    private static List<Employee> employeeResult(List<Employee> employeeList, Report report) {
        int threshold = (int) Math.min(employeeList.size(), report.getTopPerformersThreshold());
        if(threshold < 1) threshold = 1;
        employeeList.sort(Comparator.comparing(Employee::getTotalSales).reversed());
        Stack<Employee> stack = new Stack<>();

        int counter = 0;
        for (Employee employee : employeeList) {
            if(counter >= Math.round(threshold)) break;
            if(employee.getSalesPeriod() > report.getPeriodLimit()) continue;

            if(!stack.isEmpty() && stack.peek().getTotalSales() == employee.getTotalSales()) stack.add(employee);
            else {
                stack.add(employee);
                counter++;
            }
        }
        return new ArrayList<>(stack);
    }

    private static double calculateScore(Employee employee, Report report) {
        if(report.isUseExperienceMultiplier())
            return (double) (employee.getTotalSales() / employee.getSalesPeriod()) * employee.getExperienceMultiplier();
        else return (double) employee.getTotalSales() / employee.getSalesPeriod();
    }
}
