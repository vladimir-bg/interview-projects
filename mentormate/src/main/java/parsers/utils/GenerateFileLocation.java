package parsers.utils;

import java.io.File;
import java.io.IOException;

public class GenerateFileLocation {
    public static String generate(String fileName, String extension) throws IOException {
        return new File(".").getCanonicalPath() + "/" + fileName + "." + extension;
    }
}
