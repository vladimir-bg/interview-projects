package contracts;

import exceptions.DuplicateFileException;
import org.json.simple.parser.ParseException;

import java.io.IOException;

@FunctionalInterface
public interface FunctionTypeJson {
    void run(Object object) throws IOException, ParseException, DuplicateFileException;
}
