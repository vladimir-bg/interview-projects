package contracts;

import exceptions.DuplicateFileException;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;

@FunctionalInterface
public interface FunctionType {
    void run(String string) throws IOException, ParseException, DuplicateFileException;
}
