import contracts.FunctionType;
import exceptions.DuplicateFileException;
import models.Employee;
import models.Report;
import org.json.simple.parser.ParseException;
import parsers.CsvFileParser;
import parsers.JsonFileParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Report report = new Report();
        List<Employee> employees = new ArrayList<>();
        FunctionType reportFunction = (file) -> JsonFileParser.readReportFile(file, report);
        FunctionType employeeFunction = (file) -> JsonFileParser.readEmployeeFile(file, employees);
        FunctionType csvFunction = (file) -> CsvFileParser.create(report, employees, file);

        loopForChecking("Please enter the name of report json file:", reportFunction);
        loopForChecking("Please enter the name of employee json file:", employeeFunction);
        loopForChecking("Please enter name for report csv file:", csvFunction);
    }

    private static void loopForChecking(String message, FunctionType functionType) {
        Scanner scanner = new Scanner(System.in);
        String file = "";
        while (file.isEmpty()) {
            try {
                System.out.println(message);
                file = scanner.nextLine();
                functionType.run(file);
            } catch (FileNotFoundException | DuplicateFileException e) {
                System.out.println(e.getMessage());
                file = "";
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
