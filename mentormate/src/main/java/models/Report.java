package models;

public class Report {
    private long topPerformersThreshold;
    private boolean useExperienceMultiplier;
    private long periodLimit;

    public long getTopPerformersThreshold() {
        return topPerformersThreshold;
    }

    public void setTopPerformersThreshold(long topPerformersThreshold) {
        this.topPerformersThreshold = topPerformersThreshold;
    }

    public boolean isUseExperienceMultiplier() {
        return useExperienceMultiplier;
    }

    public void setUseExperienceMultiplier(boolean useExperienceMultiplier) {
        this.useExperienceMultiplier = useExperienceMultiplier;
    }

    public long getPeriodLimit() {
        return periodLimit;
    }

    public void setPeriodLimit(long periodLimit) {
        this.periodLimit = periodLimit;
    }
}
